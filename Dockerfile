FROM python
RUN mkdir /tcl
ADD ./* /tcl/
WORKDIR /tcl
RUN pip install -r requirements.txt
CMD ["./tcl-tools.py", "192.168.1.54"]