# TCL tools
Runs an API server to set TCL TV brightness and toggle game mode

Uses RokuTV API

## Usage: 

Populate `brightness.txt` and `gamemode.txt` with the TV's current values

---

### Local
Run `pip3 install -r requirements.txt`

Run `./tcl-tools.py <TV IP>`

### Docker
Run `docker build -t tcl-tools .`

Run `docker run --net=host --restart=always -d tcl-tools`

---

The API will be available on `localhost:5002`, with the following endpoints:

`/state`: Returns the game mode and brightness states

`/brightness/<BRIGHTNESS>`: Sets the brightness to the desired value

`/gamemode/<on/off>`: Toggles game-mode on or off

All endpoints are GET

Tested on 55R617

## Demos
Brightness control: https://streamable.com/b017o

Game mode toggle: https://streamable.com/h9qdj
